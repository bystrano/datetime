<?php
declare(strict_types=1);

namespace mby\DateTime\tests;

use PHPUnit\Framework\TestCase;
use mby\DateTime;

final class DateTimeTest extends TestCase
{
    public function testClassExists()
    {
        $date = new DateTime('now');

        $this->assertEquals(
            'mby\DateTime',
            get_class($date)
        );
    }

    public function testCanFormat()
    {
        $date = new DateTime('01-01-1970');

        $this->assertEquals(
            '19700101',
            $date->format('Ymd')
        );
    }
}
