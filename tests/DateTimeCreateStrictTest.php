<?php
declare(strict_types=1);

namespace mby\DateTime\tests;

use InvalidArgumentException;

use PHPUnit\Framework\TestCase;
use mby\DateTime;
use const mby\DATETIME_ERROR;
use const mby\DATETIME_WARNING;

final class DateTimeCreateStrict extends TestCase
{
    public function testValidDate()
    {
        $date = DateTime::createFromFormatStrict('!Y-m-d', '2016-10-21');

        $this->assertEquals(
            '2016-10-21 00:00:00',
            $date->format('Y-m-d H:i:s')
        );
    }

    public function testNoDate()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Couldn't parse  as 'Y-m-d' : Data missing");
        $this->expectExceptionCode(DATETIME_ERROR);

        $date = DateTime::createFromFormatStrict('Y-m-d', '');
    }

    public function testInvalidDate()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Error parsing 2016-21-10 as '!Y-m-d' : The parsed date was invalid");
        $this->expectExceptionCode(DATETIME_WARNING);

        $date = DateTime::createFromFormatStrict('!Y-m-d', '2016-21-10');
    }
}
