<?php
declare(strict_types=1);

namespace mby\DateTimeImmutable\tests;

use PHPUnit\Framework\TestCase;
use mby\DateTimeImmutable;

final class DateTimeImmutableTest extends TestCase
{
    public function testClassExists()
    {
        $date = new DateTimeImmutable('now');

        $this->assertEquals(
            'mby\DateTimeImmutable',
            get_class($date)
        );
    }

    public function testCanFormat()
    {
        $date = new DateTimeImmutable('01-01-1970');

        $this->assertEquals(
            '19700101',
            $date->format('Ymd')
        );
    }
}
