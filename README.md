This package extends the `DateTime` and `DateTimeImmutable` class.
It defines a new creation method `createFromFormatStrict` that behaves like `createFromFormat`, but throws `InvalidArgumentException` when the parsing fails.
