<?php
declare(strict_types=1);

namespace mby;

use DateTime as DateTimeCore;

class DateTime extends DateTimeCore
{
    use CreateFromFormatStrict;
}
