<?php
declare(strict_types=1);

namespace mby;

use DateTimeZone;
use InvalidArgumentException;

const DATETIME_ERROR = 1;
const DATETIME_WARNING = 2;

trait CreateFromFormatStrict
{
    /**
     * Parses a time string according to a specified format.
     *
     * Like the builtin DateTime::createDateFromFormat, but much more
     * sensitive to bad $datetime strings. Throws errors instead of being
     * "smart" like the builtin function.
     *
     * @template T of DateTimeInterface
     *
     * @param string $format The format that the passed in string should be
     *    in. See the builtin createDateFromFormat for details.
     * @param string $datetime String representing the time.
     * @param DateTimeZone|null $timezone An optional DateTimeZone object
     *    representing the desired time zone.
     *
     * @return T
     *
     * @throws InvalidArgumentException When the $datetime cannot be parsed.
     *    The error code is `DATETIME_ERROR` if the parsing didn't produce any
     *    result (eg. 'adskjh'), and `DATETIME_WARNING` if the string is
     *    invalid, but still valid enough for createFromFormat to return
     *    something (e.g. '2019-22-56')
     */
    public static function createFromFormatStrict(
        string $format,
        string $datetime,
        DateTimeZone $timezone = null
    ) {
        $date = DateTime::createFromFormat($format, $datetime, $timezone);
        $errors = DateTime::getLastErrors();

        if (($date === false) or ($errors['error_count'] > 0)) {
            throw new InvalidArgumentException(
                sprintf(
                    "Couldn't parse %s as '%s' : %s",
                    $datetime,
                    $format,
                    implode(', ', $errors['errors'])
                ),
                DATETIME_ERROR
            );
        } elseif ($errors['warning_count'] > 0) {
            throw new InvalidArgumentException(
                sprintf(
                    "Error parsing %s as '%s' : %s",
                    $datetime,
                    $format,
                    implode(', ', $errors['warnings'])
                ),
                DATETIME_WARNING
            );
        }

        return $date;
    }
}
