<?php
declare(strict_types=1);

namespace mby;

use DateTimeImmutable as DateTimeImmutableCore;

class DateTimeImmutable extends DateTimeImmutableCore
{
    use CreateFromFormatStrict;
}
