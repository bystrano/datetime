.PHONY: test  # run unit tests
.PHONY: check # run static type checking


test: vendor/bin/phpunit
	@vendor/bin/phpunit --colors=auto --bootstrap vendor/autoload.php tests/

vendor/bin/phpunit:
	@composer install --dev


check: vendor/bin/phpstan
	@vendor/bin/phpstan analyse src/ tests/

vendor/bin/phpstan:
	@composer install --dev
